﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using MadalinaM_P2.Other;
using MadalinaMierloiu.DataMap;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MadalinaM_P2.Test_Reference
{
    public class xmlnit
    {

        public static IWebDriver driver;

        public void Init()
        {
            string xml_text1 = Utils.ReadFile1(Path.Combine(Utils.FileLocation, Utils.urlXml));
            string xml_text2 = Utils.ReadFile2(Utils.regXml);

            Page Id1 = Utils.DeserilizeXml1(xml_text1);
            driver.Navigate().GoToUrl(Id1.Url);
        }
    }
}
