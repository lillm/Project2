﻿using MadalinaM_P2.Other;
using MadalinaM_P2.Pages;
using MadalinaM_P2.Test_References;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MadalinaMierloiu.DataMap;
using ConsoleApp1.Other.DataMaps;
using System.IO;

namespace MadalinaMierloiu.Tests_Data
{
    public class RegisterTest : BaseTest
    {
        /// <summary>
        /// test nou ucare efectueaza completarea campurilor de pe pagina de inregistrare cu date dintr-un xml
        /// </summary>
        [Test]
        public void Test2()
        {
            string xml_text1 = Utils.ReadFile1(Path.Combine(Utils.FileLocation, Utils.urlXml));
            string xml_text2 = Utils.ReadFile2(Utils.regXml);

            Page Id1 = Utils.DeserilizeXml1(xml_text1);
            driver.Navigate().GoToUrl(Id1.Url);

            ContNou reg = new ContNou(driver);
            Cont Id2 = Utils.DeserilizeXml2(xml_text2);
            reg.Register(Id2.Nume, Id2.Prenume, Id2.Email, Id2.Parola, Id2.Telefon, Id2.Judet, Id2.Oras, Id2.Adresa);
            Thread.Sleep(3000);
            ContNou ver = new ContNou(driver);
            ver.Verificare(Id2.Nume, Id2.Prenume);
        }
         
         
  

       

    }
}

