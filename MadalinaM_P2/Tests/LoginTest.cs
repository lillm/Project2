﻿using MadalinaM_P2.Other;
using MadalinaM_P2.Pages;
using MadalinaM_P2.Test_References;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MadalinaMierloiu.DataMap;
using ConsoleApp1.Other.DataMaps;
using System.IO;

namespace MadalinaMierloiu.Tests_Data
{
    public class LoginTest : BaseTest
    {
        /// <summary>
        /// test nou ucare efectueaza logarea cu date dintr-un xml
        /// </summary>
        [Test]
        public void Test2()
        {
            string xml_text1 = Utils.ReadFile1(Path.Combine(Utils.FileLocation, Utils.urlXml));
            string xml_text2 = Utils.ReadFile3(Utils.logXml);

            Page Id1 = Utils.DeserilizeXml1(xml_text1);
            driver.Navigate().GoToUrl(Id1.Url);

            Login log = new Login(driver);
            LoginData Id3 = Utils.DeserilizeXml3(xml_text2);
            log.Logare(Id3.Email, Id3.Parola);
            log.Ver1();
                       
        }



    }
}

