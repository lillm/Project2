﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MadalinaM_P2.Pages
{
    public class ContNou
    {
        public IWebDriver driver;
        IWebElement Nume => driver.FindElement(By.CssSelector("#newAccount-customer_lastname"));
        IWebElement Prenume => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("#newAccount-customer_firstname")));
        IWebElement Email => driver.FindElement(By.Id("newAccount-customer_email"));
        IWebElement Parola1 => driver.FindElement(By.Id("newAccount-customer_password"));
        IWebElement Parola2 => driver.FindElement(By.CssSelector("#newAccount-customer_confirm_password"));
        IWebElement Telefon => driver.FindElement(By.CssSelector("#newAccount-customer_phone"));
        IWebElement Judet => driver.FindElement(By.CssSelector("#county_id"));
        IWebElement Oras => driver.FindElement(By.CssSelector("#city_id"));
        IWebElement Adresa => driver.FindElement(By.CssSelector("#newAccount-customer_street_address"));
        IWebElement CheckBox1 => driver.FindElement(By.CssSelector("#content > div > div.col-xs-12.col-sm-6.col-md-5.account-new-left > div:nth-child(3) > div > div > div:nth-child(2) > label"));
        IWebElement Buton => driver.FindElement(By.CssSelector("#content > div > div.col-xs-12.col-sm-6.col-md-5.account-new-left > div:nth-child(4) > div > button"));
        IWebElement cookies => new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(x => x.FindElement(By.CssSelector("body > div.cookies_policy_inform_wrapper > div > span.cookies_consent_all_button")));
        IWebElement Ver => driver.FindElement(By.CssSelector("#content > div:nth-child(1) > div > h1 > span"));


        public ContNou(IWebDriver driver)
        {
            this.driver = driver;
        }

        //public void GoTo(IWebElement element)
        //{
        //    Actions actions = new Actions(driver);
        //    actions.MoveToElement(element);
        //    actions.Perform();
        //    actions.SendKeys(Keys.PageDown);
        //}
        

        public void Register(string nume, string prenume, string mail, string parola, string telefon, string judet, string oras, string adresa)
        {

            cookies.Click();

            Nume.Clear();
            Nume.SendKeys(nume);

            Prenume.Clear();
            Prenume.SendKeys(prenume);

            Email.Clear();
            Email.SendKeys(mail);

            Parola1.Clear();
            Parola1.SendKeys(parola);

            Parola2.Clear();
            Parola2.SendKeys(parola);

            Telefon.Clear();
            Telefon.SendKeys(telefon);

            Judet.Click();
            Judet.SendKeys(judet);
            Judet.Click();
            Thread.Sleep(3000);
            
            Oras.Click();
            Oras.SendKeys(oras);
            Oras.Click();
            
            Adresa.Clear();
            Adresa.SendKeys(adresa);
            
            CheckBox1.Click();
                
            Buton.Click();
                
            }

        public void Verificare(string prenume, string nume)
        {
            Assert.AreEqual(Ver.Text, "Administrare cont - "+prenume+nume);
        }

    }

    }

