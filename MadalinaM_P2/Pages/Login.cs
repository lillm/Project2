﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MadalinaM_P2.Pages
{
    public class Login
    {
        public IWebDriver driver;

        IWebElement ContulMeu => driver.FindElement(By.LinkText("Contul meu"));
        IWebElement Email => driver.FindElement(By.Id("email"));
        IWebElement Parola => driver.FindElement(By.Id("parola"));
        IWebElement Autentificare => driver.FindElement(By.LinkText("Autentificare"));
        IWebElement Iesire => driver.FindElement(By.LinkText("Iesire"));
        IWebElement VerLogout => driver.FindElement(By.ClassName("status"));

        public Login(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void Logare(string mail, string parola)
        {
            ContulMeu.Click();

            Email.Clear();
            Email.SendKeys(mail);

            Parola.Clear();
            Parola.SendKeys(parola);
            Parola.Submit();
            
            
        }

        public void LogO()
        {
            Iesire.Click();

        }

        public void Ver1()
        {
            Assert.AreEqual(Iesire.Text, "Iesire");

        }

        public void Ver2()
        {
            Assert.AreEqual(VerLogout.Text, "Log out success");

        }

    }

}

