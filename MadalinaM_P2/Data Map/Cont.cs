﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MadalinaMierloiu.DataMap
{
    public class Cont
    {
        public string Nume { get; set; }
        public string Prenume { get; set; }
        public string Email { get; set; }
        public string Parola { get; set; }
        public string Telefon { get; set; }
        public string Judet { get; set; }
        public string Oras { get; set; }
        public string Adresa { get; set; }
    }
}