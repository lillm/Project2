﻿using MadalinaM_P2.Pages;
using MadalinaMierloiu.DataMap;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MadalinaM_P2.Other
{
    /// <summary>
    /// configurarea coneziunii de DB
    /// </summary>
    class Utils
    {
       
        public static string FileLocation = String.Format("{0}",Properties.Settings.Default.DataFilesLocation);
        public static string regXml = String.Format("{0}\\{1}", Properties.Settings.Default.DataFilesLocation, Properties.Settings.Default.RegData);
        public static string urlXml = String.Format("{0}", Properties.Settings.Default.UrlData);
        public static string logXml = String.Format("{0}\\{1}", Properties.Settings.Default.DataFilesLocation, Properties.Settings.Default.LoginData);


        /// <summary>
        /// metode folosite pentru citirea datelor din fisierele cu date pentru teste
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string ReadFile1(string file)
        {
            string filePath = Path.Combine(FileLocation, urlXml);
            return File.ReadAllText(file);
            
        }

        public static string ReadFile2(string file)
        {
            string filePath = regXml;
            return File.ReadAllText(file);

        }

        public static string ReadFile3(string file)
        {
            string filePath = logXml;
            return File.ReadAllText(file);

        }
        /// <summary>
        /// interpretarea datelor din fisierele cu date
        /// </summary>
        /// <param name="xml_text"></param>
        /// <returns></returns>
        /// 
        public static Page DeserilizeXml1(string xml_text)
        {
            // Root name = numele fisierului xml

            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                ElementName = "Page",
                IsNullable = true
            };
            StringReader sr = new StringReader(xml_text);
            XmlSerializer xs = new XmlSerializer(typeof(Page), xRoot);
            return (Page)xs.Deserialize(sr);

        }

        public static Cont DeserilizeXml2(string xml_text)
        {
            // Root name = numele fisierului xml

            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                ElementName = "Cont",
                IsNullable = true
            };
            StringReader sr = new StringReader(xml_text);
            XmlSerializer xs = new XmlSerializer(typeof(Cont), xRoot);
            return (Cont)xs.Deserialize(sr);

        }


        public static LoginData DeserilizeXml3(string xml_text)
        {
            // Root name = numele fisierului xml

            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                ElementName = "LoginData",
                IsNullable = true
            };
            StringReader sr = new StringReader(xml_text);
            XmlSerializer xs = new XmlSerializer(typeof(LoginData), xRoot);
            return (LoginData)xs.Deserialize(sr);

        }
        
        
       
    }
}